﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgrammingExercises2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //Console.WriteLine("Enter the limit number: ");        //Fibonacci numbers
            //Fibonacci(Int32.Parse(Console.ReadLine()));

            //Console.WriteLine("Enter your name (Alice and Bob are recognized): ");
            //HelloAliceOrBob(Console.ReadLine().ToLower());


            //Console.WriteLine("Enter word to check if it is palindrome..");       //Check if word is palindrome
            //Palindrome(Console.ReadLine());

            //string input = "aasssdda";      //String compression e.g. "aasssdddda" to "a2s3d4a1"
            //StringCompression(input);

            //Console.WriteLine("Enter max prime number..");      //Generates list of prime numbers smaller than entered number
            //GeneratePrimes(Int32.Parse(Console.ReadLine()));

            //Console.WriteLine("Enter number to check if it is prime number..");
            //CheckIfPrimeNumberGame(Int32.Parse(Console.ReadLine()));

            DutchFlag();

            Console.ReadLine();
        }

        private static void DutchFlag()
        {
            string balls = "WWWRRBBBBRRBW";
            List<string> whites = new List<string>();
            List<string> blues = new List<string>();
            List<string> reds = new List<string>();

            foreach (var item in balls)
            {
                if (item.Equals('W'))
                {
                    whites.Add(item.ToString());
                }
                else if (item.Equals('B'))
                {
                    blues.Add(item.ToString());
                }
                else if (item.Equals('R'))
                {
                    reds.Add(item.ToString());
                }
                else
                {
                    Console.WriteLine("The ball color doesnt match Dutch flag color");
                }
            }

            //for (int i = 0; i < balls.Length; i++)
            //{
            //    if (balls[i].Equals('W'))
            //    {
            //        whites.Add(balls[i].ToString());
            //    }
            //    else if (balls[i].Equals('B'))
            //    {
            //        blues.Add(balls[i].ToString());
            //    }
            //    else if (balls[i].Equals('R'))
            //    {
            //        reds.Add(balls[i].ToString());
            //    }
            //    else
            //    {
            //        Console.WriteLine("The ball color doesnt match Dutch flag color");
            //    }
            //}

            List<string> DutchFlagList = new List<string>();
            DutchFlagList.AddRange(reds);
            DutchFlagList.AddRange(whites);
            DutchFlagList.AddRange(blues);

            foreach (var item in DutchFlagList)
            {
                Console.Write(item);
            }
        }

        private static void CheckIfPrimeNumberGame(int number)
        {
           int counter = 0;
            for (int i = 1; i <= number; i++)
            {
                if (number % i == 0)
                {
                    counter++;
                }
            }
            if (counter == 2)
            {
                Console.WriteLine($"Number {number} is a prime number!");
            }
            else
            {
                Console.WriteLine($"Number {number} is NOT a prime number!");
            }

            Console.WriteLine("Try again? Y/N");
            string answer = Console.ReadLine().ToUpper();
            if (answer == "Y")
            {
                Console.WriteLine("Enter number to check if it is prime number..");
                CheckIfPrimeNumberGame(Int32.Parse(Console.ReadLine()));
            }

        }

        private static void GeneratePrimes(int n)
        {
            var r = from i in Enumerable.Range(2, n - 1).AsParallel()
                    where Enumerable.Range(2, (int)Math.Sqrt(i)).All(j => i % j != 0)
                    orderby i
                    select i;
            List<int> primes =  r.ToList();
            foreach (var item in primes)
            {
                Console.WriteLine(item);
            }
        }

        private static void StringCompression(string input)
        {
            var counter = 1;

            for (int i = 0; i < input.Length - 1; i++)
            {
                if (input[i] == input[i + 1])
                {
                    counter++;
                }
                else if (input[i] != input[i + 1])
                {
                    Console.Write(input[i].ToString() + counter);
                    counter = 1;
                }
            }
            Console.Write(input[input.Length - 1].ToString() + counter);
        }

        private static void Palindrome(string input)
        {
            var reverse = new string(input.Reverse().ToArray()) ;
            if (input.ToLower().Equals(reverse.ToLower()))
            {
                Console.WriteLine($"String {input} is Palindrome!");
                Console.WriteLine($"{input} = {reverse}");
            }
            else
            {
                Console.WriteLine($"String {input} is NOT Palindrome!");
                Console.WriteLine($"{input} != {reverse}");
            }
        }

        private static void HelloAliceOrBob(string inputName)
        {
            if (inputName.Equals("alice"))
            {
                Console.WriteLine("Hello Alice!");
            }
            else if (inputName.Equals("bob"))
            {
                Console.WriteLine("Hello Bob!");
            }
            else
            {
                Console.WriteLine("Hello Guest!");
            }
        }

        private static void Fibonacci(int limit)
        {
            int x = 0;
            int y = 1;
            for (int i = 0; i < limit; i++)
            {
                int z = x;
                Console.WriteLine(x);
                x = y;
                y += z;
                if (x > limit)
                {
                    break;
                }
            }
        }
    }
}
